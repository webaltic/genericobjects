<?php

namespace Webaltic\GenericObjects;

interface ValueInterface
{
    public function getAmount(): float;

    public function getCurrency(): string;
}
