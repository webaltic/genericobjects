<?php

namespace Webaltic\GenericObjects;

interface RecurringTransactionInterface extends TransactionInterface
{
    public function getOriginalTransaction(): RecurringTransactionInterface;
}
