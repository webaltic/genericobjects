<?php

namespace Webaltic\GenericObjects;

interface ErrorInterface
{
    public function getMessage(): string;

    public function getCode(): string;

    public function getCategory(): string;
}
