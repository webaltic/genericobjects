<?php

namespace Webaltic\GenericObjects;

interface PaymentProviderInterface
{
    public function getName(): string;

    public function getConfiguration(): array;
}