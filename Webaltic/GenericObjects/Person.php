<?php

namespace Webaltic\GenericObjects;

/**
 * @package Webaltic\GenericObjects
 */
class Person implements PersonInterface, ExtraDataInterface
{
    use ExtraDataTrait;

    protected string              $id;
    protected string              $gender;
    protected ?\DateTimeInterface $birthDate;
    protected string              $email;
    protected string              $firstName;
    protected string              $middleName;
    protected string              $lastName;
    protected string              $languageCode;
    protected string              $countryCode;
    protected string              $ipAddress;
    protected string              $phoneNumber;
    protected ?AddressInterface   $shippingAddress = null;
    protected ?AddressInterface   $billingAddress  = null;

    public function __construct(
        string              $id, string $email,
        string              $firstName = '', string $middleName = '', string $lastName = '',
        string              $languageCode = '', string $countryCode = '',
        string              $ipAddress = '',
        ?\DateTimeInterface $birthDate = null,
        string              $gender = PersonInterface::GENDER_UNKNOWN,
        string              $phoneNumber = '',
        ?AddressInterface   $shippingAddress = null,
        ?AddressInterface   $billingAddress = null,
        array               $extraData = []
    ) {
        $this->id              = $id;
        $this->gender          = $gender;
        $this->birthDate       = $birthDate;
        $this->email           = $email;
        $this->firstName       = $firstName;
        $this->middleName      = $middleName;
        $this->lastName        = $lastName;
        $this->languageCode    = $languageCode;
        $this->countryCode     = $countryCode;
        $this->ipAddress       = $ipAddress;
        $this->phoneNumber     = $phoneNumber;
        $this->shippingAddress = $shippingAddress;
        $this->billingAddress  = $billingAddress;
        $this->extraData       = $extraData;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getShippingAddress(): ?AddressInterface
    {
        return $this->shippingAddress;
    }

    public function getBillingAddress(): ?AddressInterface
    {
        return $this->billingAddress;
    }

}