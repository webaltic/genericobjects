<?php

namespace Webaltic\GenericObjects;

interface ReasonInterface
{
    public function getCode(): string;

    public function getReasonText(): string;
}