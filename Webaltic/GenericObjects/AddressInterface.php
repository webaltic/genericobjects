<?php

namespace Webaltic\GenericObjects;

interface AddressInterface
{
    public function getCountryCode(): string;

    public function getState(): string;

    public function getCity(): string;

    public function getPostCode(): string;

    public function getStreetLine1(): string;

    public function getStreetLine2(): string;

    public function getStreetLine3(): string;
}
