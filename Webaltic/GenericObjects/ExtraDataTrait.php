<?php

namespace Webaltic\GenericObjects;

trait ExtraDataTrait
{
    protected array $extraData;

    public function getExtraData(): array
    {
        return $this->extraData;
    }

    public function getExtraDataItem(string $extraDataItemName, $defaultValue = null)
    {
        return $this->hasExtraDataItem($extraDataItemName) ? $this->extraData[$extraDataItemName] : $defaultValue;
    }

    public function hasExtraDataItem(string $extraDataItemName): bool
    {
        return array_key_exists($extraDataItemName, $this->extraData);
    }
}
