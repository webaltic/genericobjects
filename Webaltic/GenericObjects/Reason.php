<?php

namespace Webaltic\GenericObjects;

class Reason implements ReasonInterface
{
    use ExtraDataTrait;

    protected string $code;
    protected string $reasonText;

    public function __construct(string $code, string $reasonText, array $extraData = [])
    {
        $this->code       = $code;
        $this->reasonText = $reasonText;
        $this->extraData  = $extraData;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getReasonText(): string
    {
        return $this->reasonText;
    }

}