<?php

namespace Webaltic\GenericObjects;

class RecurringTransaction extends Transaction implements RecurringTransactionInterface
{
    protected RecurringTransactionInterface $originalTransaction;

    public function __construct(
        RecurringTransactionInterface $originalTransaction,
        string                        $id,
        string                        $merchantReference,
        string                        $paymentReference,
        int                           $status,
        \DateTimeInterface            $transactionDate,
        string                        $type,
        string                        $merchantId,
        bool                          $is3Ds,
        PaymentProviderInterface      $paymentProvider,
        ValueInterface                $value,
        array                         $extraData = []
    ) {
        $this->originalTransaction = $originalTransaction;

        parent::__construct($id, $merchantReference, $paymentReference, $status, $transactionDate, $type, $merchantId, $is3Ds, $paymentProvider, $value);
    }

    public function getOriginalTransaction(): RecurringTransactionInterface
    {
        return $this->originalTransaction;
    }

}
