<?php

namespace Webaltic\GenericObjects;

class CallResponse implements CallResponseInterface
{
    /**
     * Response that would normally be returned
     *
     * @var mixed
     */
    protected $response;

    // A message regarding response - if response is failure, then message would return error message; If success - optionally it might contain a feedback;
    protected string $message;

    // Any extra information that might be required to be passed from a service to an outside
    protected array $extraDetails;

    public function __construct($response, string $message = '', $extraDetails = [])
    {
        $this->response     = $response;
        $this->message      = $message;
        $this->extraDetails = $extraDetails;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getExtraDetails(): array
    {
        return $this->extraDetails;
    }

    public function getDetail($name)
    {
        if (array_key_exists($name, $this->extraDetails)) {
            return $this->extraDetails[$name];
        }

        return null;
    }

    public function __toString(): string
    {
        return $this->message;
    }

    public function isSuccess(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_SUCCESS;
    }

    public function isFailure(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_FAILURE;
    }

    public function isDuplicate(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_DUPLICATE;
    }

    public function isComponentError(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_COMPONENT_ERROR;
    }

    public function isRemoteError(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_REMOTE_ERROR;
    }

    public function isInvalidArgumentError(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_INVALID_ARGUMENT;
    }

    public function isNotFoundError(): bool
    {
        return $this->getResponse() === CallResponseInterface::RESPONSE_NOT_FOUND;
    }

}
