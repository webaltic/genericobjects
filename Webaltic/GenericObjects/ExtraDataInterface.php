<?php

namespace Webaltic\GenericObjects;

/**
 * An interface that asks object to implement possibility of getting of extra data item(s), if required
 */
interface ExtraDataInterface
{
    public function getExtraData(): array;

    public function getExtraDataItem(string $extraDataItemName, $defaultValue = null);

    public function hasExtraDataItem(string $extraDataItemName): bool;
}
