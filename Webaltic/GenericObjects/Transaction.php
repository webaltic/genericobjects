<?php

namespace Webaltic\GenericObjects;

class Transaction implements TransactionInterface, ExtraDataInterface
{
    use ExtraDataTrait;

    protected string                   $id;
    protected string                   $merchantReference;
    protected string                   $paymentReference;
    protected int                      $status;
    protected string                   $type;
    protected ValueInterface           $value;
    protected string                   $merchantId;
    protected bool                     $is3Ds;
    protected PaymentProviderInterface $paymentProvider;
    protected \DateTimeInterface       $transactionDate;

    public function __construct(
        string                   $id,
        string                   $merchantReference,
        string                   $paymentReference,
        int                      $status,
        \DateTimeInterface       $transactionDate,
        string                   $type,
        string                   $merchantId,
        bool                     $is3Ds,
        PaymentProviderInterface $paymentProvider,
        ValueInterface           $value,
        array                    $extraData = []
    ) {
        $this->id                = $id;
        $this->merchantReference = $merchantReference;
        $this->paymentReference  = $paymentReference;
        $this->status            = $status;
        $this->transactionDate   = $transactionDate;
        $this->type              = $type;
        $this->merchantId        = $merchantId;
        $this->is3Ds             = $is3Ds;
        $this->paymentProvider   = $paymentProvider;
        $this->value             = $value;
        $this->extraData         = $extraData;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getMerchantReference(): string
    {
        return $this->merchantReference;
    }

    public function getPaymentReference(): string
    {
        return $this->paymentReference;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getValue(): ValueInterface
    {
        return $this->value;
    }

    public function getMerchantId(): string
    {
        return $this->merchantId;
    }

    public function is3Ds(): bool
    {
        return $this->is3Ds;
    }

    public function getPaymentProvider(): PaymentProviderInterface
    {
        return $this->paymentProvider;
    }

    public function getTransactionDate(): \DateTimeInterface
    {
        return $this->transactionDate;
    }

}
