<?php

namespace Webaltic\GenericObjects;

class Address implements AddressInterface, ExtraDataInterface
{
    use ExtraDataTrait;

    protected string $countryCode;
    protected string $state;
    protected string $city;
    protected string $postCode;
    protected string $streetLine1;
    protected string $streetLine2;
    protected string $streetLine3;

    public function __construct(
        string $countryCode, string $state, string $city, string $postCode,
        string $streetLine1, string $streetLine2 = '', string $streetLine3 = '',
        array  $extraData = []
    ) {
        $this->countryCode = $countryCode;
        $this->state       = $state;
        $this->city        = $city;
        $this->postCode    = $postCode;
        $this->streetLine1 = $streetLine1;
        $this->streetLine2 = $streetLine2;
        $this->streetLine3 = $streetLine3;
        $this->extraData   = $extraData;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getPostCode(): string
    {
        return $this->postCode;
    }

    public function getStreetLine1(): string
    {
        return $this->streetLine1;
    }

    public function getStreetLine2(): string
    {
        return $this->streetLine2;
    }

    public function getStreetLine3(): string
    {
        return $this->streetLine3;
    }

}
