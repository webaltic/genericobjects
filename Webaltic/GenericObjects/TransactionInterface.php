<?php

namespace Webaltic\GenericObjects;

interface TransactionInterface
{
    public function getId(): string;

    public function getMerchantReference(): string;

    public function getPaymentReference(): string;

    public function getStatus(): int;

    public function getType(): string;

    public function getValue(): ValueInterface;

    public function getMerchantId(): string;

    public function is3Ds(): bool;

    public function getPaymentProvider(): PaymentProviderInterface;

    public function getTransactionDate(): \DateTimeInterface;

}
