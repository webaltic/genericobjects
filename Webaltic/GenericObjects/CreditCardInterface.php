<?php

namespace Webaltic\GenericObjects;

interface CreditCardInterface
{
    public function getCardNumber(): string;

    public function getExpiryYear(): string;

    public function getExpiryMonth(): string;

    public function getCvv(): string;

    public function getHolderName(): string;

    public function getCardBin(): string;

    public function getExpiryYearShort(): string;

}
