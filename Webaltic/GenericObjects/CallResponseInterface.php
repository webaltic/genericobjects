<?php

namespace Webaltic\GenericObjects;

interface CallResponseInterface
{
    /**
     * Service method completed its task successfully
     */
    public const RESPONSE_SUCCESS = 1;

    /**
     * Service method failed while doing its task
     */
    public const RESPONSE_FAILURE = 0;

    /**
     * Service method encountered duplicate item while doing its task
     */
    public const RESPONSE_DUPLICATE = -1;

    /**
     * Service method encountered component error while doing its task
     */
    public const RESPONSE_COMPONENT_ERROR = -2;

    /**
     * Service method encountered remote error while doing its task. Could happen if service is communicating with remote 3rd party provider(s).
     */
    public const RESPONSE_REMOTE_ERROR = -3;

    /**
     * Service method encountered invalid argument error while doing its task. E.g. creating an item, where name contains invalid characters
     */
    public const RESPONSE_INVALID_ARGUMENT = -4;

    /**
     * Service method encountered 'item not found' error while doing its task.
     */
    public const RESPONSE_NOT_FOUND = -5;

    public function getResponse();

    public function getMessage(): string;

    public function getExtraDetails(): array;

    public function getDetail($name);

    public function isSuccess(): bool;

    public function isFailure(): bool;

    public function isDuplicate(): bool;

    public function isComponentError(): bool;

    public function isRemoteError(): bool;

    public function isInvalidArgumentError(): bool;

    public function isNotFoundError(): bool;
}
