<?php

namespace Webaltic\GenericObjects;

class CreditCard implements CreditCardInterface, ExtraDataInterface
{
    use ExtraDataTrait;

    protected string $cardNumber;
    protected string $expiryYear;
    protected string $expiryMonth;
    protected string $cvv;
    protected string $holderName;

    public function __construct(
        string $cardNumber, string $expiryYear, string $expiryMonth, string $cvv, string $holderName, array $extraData = []
    ) {
        $expiryMonth = (int) $expiryMonth;
        $expiryMonth = (string) ($expiryMonth < 10 ? '0' . $expiryMonth : $expiryMonth);

        $this->cardNumber  = $cardNumber;
        $this->expiryYear  = $expiryYear;
        $this->expiryMonth = $expiryMonth;
        $this->cvv         = $cvv;
        $this->holderName  = $holderName;
        $this->extraData   = $extraData;
    }

    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }

    public function getExpiryYear(): string
    {
        return $this->expiryYear;
    }

    public function getExpiryMonth(): string
    {
        return $this->expiryMonth;
    }

    public function getCvv(): string
    {
        return $this->cvv;
    }

    public function getHolderName(): string
    {
        return $this->holderName;
    }

    public function getCardBin(): string
    {
        return substr($this->cardNumber, 0, 6);
    }

    public function getExpiryYearShort(): string
    {
        return strlen($this->expiryYear) === 4 ? substr($this->expiryYear, 2, 2) : $this->expiryYear;
    }

    public function getExpiryYearLong(): string
    {
        return strlen($this->expiryYear) === 2 ? ($this->expiryYear < 99 ? '20' : '21') . $this->expiryYear : $this->expiryYear;
    }

    public function getExpiryDate(): string
    {
        return $this->getExpiryMonth() . $this->getExpiryYearShort();
    }

}
