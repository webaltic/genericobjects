<?php

namespace Webaltic\GenericObjects;

class PaymentProvider implements PaymentProviderInterface, ExtraDataInterface
{
    use ExtraDataTrait;

    protected string $name;
    protected array  $configuration;

    public function __construct(string $name, array $configuration, array $extraData = [])
    {
        $this->name          = $name;
        $this->configuration = $configuration;
        $this->extraData     = $extraData;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getConfiguration(): array
    {
        return $this->configuration;
    }

}