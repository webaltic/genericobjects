<?php

namespace Webaltic\GenericObjects;

interface PersonInterface
{
    public const GENDER_MALE    = 'm';
    public const GENDER_FEMALE  = 'f';
    public const GENDER_OTHER   = 'o';
    public const GENDER_UNKNOWN = '-';

    public function getId(): string;

    public function getGender(): string;

    public function getBirthDate(): ?\DateTimeInterface;

    public function getEmail(): string;

    public function getFirstName(): string;

    public function getMiddleName(): string;

    public function getLastName(): string;

    public function getLanguageCode(): string;

    public function getCountryCode(): string;

    public function getIpAddress(): string;

    public function getPhoneNumber(): string;

    public function getShippingAddress(): ?AddressInterface;

    public function getBillingAddress(): ?AddressInterface;
}
