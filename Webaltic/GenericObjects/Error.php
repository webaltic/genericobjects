<?php

namespace Webaltic\GenericObjects;

class Error implements ErrorInterface
{
    protected string $message;
    protected string $code;
    protected string $category;

    public function __construct(string $message, string $code, string $category)
    {
        $this->message  = $message;
        $this->code     = $code;
        $this->category = $category;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getCategory(): string
    {
        return $this->category;
    }
}
